// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Cluster/DisplayClusterClusterEvent.h"
#include "Cluster/IDisplayClusterClusterManager.h"
#include "CaveGunBase.generated.h"

UCLASS()
class CAVEGUN_API ACaveGunBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACaveGunBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent, Category="Cave Gun") void OnPump(bool Pressed);
	UFUNCTION(BlueprintNativeEvent, Category="Cave Gun") void OnReload(bool Pressed);
	UFUNCTION(BlueprintNativeEvent, Category="Cave Gun") void OnTrigger(bool Pressed);
	
	void OnPump_Implementation(bool Pressed);
	void OnReload_Implementation(bool Pressed);
	void OnTrigger_Implementation(bool Pressed);

private:
	//Cluster Events
	FOnClusterEventListener ClusterEventListenerDelegate;
	void HandleClusterEvent(const FDisplayClusterClusterEvent& Event);

	void AttachToClusterComponent();

	bool bPumpPressed = false;
	bool bReloadPressed = false;
	bool bTriggerPressed = false;
};
