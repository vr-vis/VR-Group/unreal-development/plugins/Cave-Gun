// Fill out your copyright notice in the Description page of Project Settings.


#include "CaveGunBase.h"
#include "IDisplayCluster.h"
#include "VirtualRealityUtilities.h"

// Sets default values
ACaveGunBase::ACaveGunBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACaveGunBase::BeginPlay()
{
	Super::BeginPlay();

	//Register Cluster Events
	IDisplayClusterClusterManager* ClusterManager = IDisplayCluster::Get().GetClusterMgr();
	if (ClusterManager && !ClusterEventListenerDelegate.IsBound())
	{
		ClusterEventListenerDelegate = FOnClusterEventListener::CreateUObject(this, &ACaveGunBase::HandleClusterEvent);
		ClusterManager->AddClusterEventListener(ClusterEventListenerDelegate);
	}

	AttachToClusterComponent();
}

void ACaveGunBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	//Deregister Cluster Events
	IDisplayClusterClusterManager* ClusterManager = IDisplayCluster::Get().GetClusterMgr();
	if (ClusterManager && ClusterEventListenerDelegate.IsBound())
	{
		ClusterManager->RemoveClusterEventListener(ClusterEventListenerDelegate);
	}

	Super::EndPlay(EndPlayReason);
}

// Called every frame
void ACaveGunBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	AttachToClusterComponent();
}

void ACaveGunBase::OnPump_Implementation(bool Pressed){}

void ACaveGunBase::OnReload_Implementation(bool Pressed){}

void ACaveGunBase::OnTrigger_Implementation(bool Pressed){}

void ACaveGunBase::HandleClusterEvent(const FDisplayClusterClusterEvent& Event)
{
	if (!Event.Category.Equals("CAVEGun") || !Event.Type.Equals("ButtonChange")) return;

	if(Event.Parameters.Contains("Pump")){
		bool Status = Event.Parameters["Pump"].Equals("1")?true:false;
		if(bPumpPressed ^ Status){
			bPumpPressed = Status;
			OnPump(bPumpPressed);
		}
	} else if(Event.Parameters.Contains("Reload")){
		bool Status = Event.Parameters["Reload"].Equals("1")?true:false;
		if(bReloadPressed ^ Status){
			bReloadPressed = Status;
			OnReload(bReloadPressed);
		}
	} else if(Event.Parameters.Contains("Trigger")){
		bool Status = Event.Parameters["Trigger"].Equals("1")?true:false;
		if(bTriggerPressed ^ Status){
			bTriggerPressed = Status;
			OnTrigger(bTriggerPressed);
		}
	}
}

void ACaveGunBase::AttachToClusterComponent()
{
	static bool bAttached = false;
	if(bAttached) return;
	
	UDisplayClusterSceneComponent* CaveGunComponent = UVirtualRealityUtilities::GetClusterComponent("CaveGun");
	if(CaveGunComponent){
		AttachToComponent(CaveGunComponent, FAttachmentTransformRules::SnapToTargetIncludingScale);
		bAttached = true;
	}
}
